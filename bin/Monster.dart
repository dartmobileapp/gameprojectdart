import 'Healable.dart';
import 'Object.dart';

class Monster extends Healable implements Object {
  int dmg = 10;
  int health = 300;
  String? name = "Tang";
  int damageUltimate = 20;
  int healPower = 20;


  Monster() {
    this.name = name;
    this.health = health;
    this.dmg = dmg;
    this.damageUltimate = damageUltimate;
    this.healPower = healPower;
  }

  @override
  attacked(int damaged) {
    health = health - damaged;
    print("Monster was attacked by hero " + damaged.toString() + " damage");
    return health;
  }

  @override
  die() {
    if (health <= 0) {
      return true;
    } else {
      return false;
    }
  }

  gotUltimate(int damageUltimate, String nameUltimate) {
    health = health - damageUltimate;
    print("Monster was attacked by ultimate" + "(" + nameUltimate + ")" + " from hero " + damageUltimate.toString() + " damage");
    return health;
  }

  heal() {
    health = health + healPower;
    return health;
  }
}
