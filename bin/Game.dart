import 'dart:io';

import 'Dragon.dart';
import 'Object.dart';
import 'Hero.dart';
import 'Monster.dart';
import 'Swordsman.dart';
import 'Flyable.dart';
import 'Healable.dart';

class Game {

  int round = 0;

  run() {
    print('\n█▀▀ █ █▀▀ █░█ ▀█▀   █░█░█ █ ▀█▀ █░█   █▀▄▀█ █▀█ █▄░█ █▀ ▀█▀ █▀▀ █▀█');
    print('█▀░ █ █▄█ █▀█ ░█░   ▀▄▀▄▀ █ ░█░ █▀█   █░▀░█ █▄█ █░▀█ ▄█ ░█░ ██▄ █▀▄');
    typeOfHero();
    int n = int.parse(stdin.readLineSync()!);
    selectedHero(n);
  }

  typeOfHero() {
    print("\nChoose a hero");
    print("1. Dragon");
    print("2. Swordsman");
  }

  typeOfDamage() {
    print("\nSelect hero attack :");
    print("1. Normal Attack");
    print("2. Skill ultimate");
  }

  showHPDragon(Dragon dragon, Monster monster) {
    print("\nStatus :");
    if (dragon.health <= 0 || monster.health <= 0) {
      if (dragon.die()) {
        dragon.health = 0;
        print("HP (Dragon) " +dragon.name.toString() +" = " +dragon.health.toString());
        print("HP (Monster) " +monster.name.toString() +" = " +monster.health.toString());
      } else {
        monster.health = 0;
        print("HP (Dragon) " +dragon.name.toString() +" = " +dragon.health.toString());
        print("HP (Monster) " +monster.name.toString() +" = " +monster.health.toString());
      }
    } else {
      print("HP (Dragon) " +dragon.name.toString() +" = " +dragon.health.toString());
      print("HP (Monster) " +monster.name.toString() +" = " +monster.health.toString());
    }
  }

  showHPSwordsman(Swordsman swordsman, Monster monster) {
    print("\nStatus :");
    if (swordsman.health <= 0 || monster.health <= 0) {
      if (swordsman.die()) {
        swordsman.health = 0;
        print("HP (Swordsman) " +swordsman.name.toString() +" = " +swordsman.health.toString());
        print("HP (Monster) " +monster.name.toString() + " = " +monster.health.toString());
      } else {
        monster.health = 0;
        print("HP (Swordsman) " +swordsman.name.toString() +" = " +swordsman.health.toString());
        print("HP (Monster) " +monster.name.toString() +" = " +monster.health.toString());
      }
    } else {
      print("HP (Swordsman) " +swordsman.name.toString() +" = " +swordsman.health.toString());
      print("HP (Monster) " +monster.name.toString() +" = " +monster.health.toString());
    }
  }

  selectedHero(int n) {
    switch (n) {
      case 1:
        print("\nInput dragon name : ");
        String name = stdin.readLineSync()!;
        Dragon dragon = new Dragon(name);
        Monster monster = new Monster();
        print(dragon.name.toString() + " VS " + monster.name.toString());

        //show health
        showHPDragon(dragon, monster);

        while (!dragon.die() && !monster.die()) {
          //Check die
          if (dragon.die() == true || monster.die() == true) {
            break;
          }

          typeOfDamage();

          n = int.parse(stdin.readLineSync()!);
          print("\nYour hero attacks dragon !!!");
          if (n == 1) {
            monster.attacked(dragon.dmg);
          } else {
            dragon.fly();
            monster.gotUltimate(dragon.damageUltimate, dragon.nameUltimate);
          }

          //show health
          showHPDragon(dragon, monster);

        //Check HP
          if (dragon.die() == true || monster.die() == true) {
            break;
          }

          //Bot attack
          print("\nMonster attacks your hero !!!");
          dragon.attacked(monster.dmg);
          round++;

          //show health
          showHPDragon(dragon, monster);

          //Check heal
          if (round % 4 == 0) {
            monster.heal();
            print("\nMonster increase HP " + monster.healPower.toString());

          //show health
          showHPDragon(dragon, monster);

          }

          //Check Ultimate Monster to attack Hero
          if (round % 3 == 0) {
            dragon.gotUltimate(monster.damageUltimate);

          //show health
          showHPDragon(dragon, monster);
          }
        }

        //Check win
        if (dragon.die()) {
          print("\nMonster is win !!!");
        } else {
          print("\n" + dragon.name.toString() + " is win !!!");
        }

      break;

      case 2:
        print("\nInput swordsman name : ");
        String name = stdin.readLineSync()!;
        Swordsman swordsman = new Swordsman(name);
        Monster monster = new Monster();
        print(swordsman.name.toString() + " VS " + monster.name.toString());

        //show health
        showHPSwordsman(swordsman, monster);

        while (!swordsman.die() && !monster.die()) {
          //Check HP
          if (swordsman.die() == true || monster.die() == true) {
            break;
          }

          typeOfDamage();
          n = int.parse(stdin.readLineSync()!);
          print("\nYour hero attacks dragon !!!");
          if (n == 1) {
            monster.attacked(swordsman.dmg);
          } else {
            monster.gotUltimate(swordsman.damageUltimate, swordsman.nameUltimate);
          }

          //show health
          showHPSwordsman(swordsman, monster);

          //Check die
          if (swordsman.die() == true || monster.die() == true) {
            break;
          }

          //Bot attack
          print("\nMonster attacks your hero !!!");
          swordsman.attacked(monster.dmg);
          round++;

          //showHP
          showHPSwordsman(swordsman, monster);

          //Check heal
          if (round % 4 == 0) {
            monster.heal();
            print("\nMonster increase HP " + monster.healPower.toString());
            //showHP
            showHPSwordsman(swordsman, monster);
          }

          //Check Ultimate Monster to attack Hero
          if (round % 5 == 0) {
            swordsman.gotUltimate(monster.damageUltimate);
            //showHP
            showHPSwordsman(swordsman, monster);
          }

          
        }

        //Check win
        if (swordsman.die()) {
          print("\nMonster is win !!!");
        } else {
          print("\n" + swordsman.name.toString() + " is win !!!");
        }

      break;

    }
}
}

void main(List<String> args) {
  Game game = new Game();
  game.run();
  
}


