import 'dart:io';

import 'Hero.dart';
import 'Object.dart';

class Swordsman implements Hero, Object {
  int dmg = 40;
  int health = 100;
  String? name;
  int damageUltimate = 120;
  String nameUltimate = "Dragon's Lane";

  Swordsman(String name) {
    this.name = name;
    this.health = health;
    this.dmg = dmg;
    this.damageUltimate;
  }

  @override
  attacked(int damaged) {
    health = health - damaged;
    print("Hero was attacked by monster " + damaged.toString() + " damage");
    return health;
  }

  @override
  die() {
    if (health <= 0) {
      return true;
    } else {
      return false;
    }
  }

  @override
  gotUltimate(int damageUltimate) {
    health = health - damageUltimate;
    print("Hero was attacked by ultimate from monster " +
        damageUltimate.toString() +
        " damage");
    return health;
  }
}
