import 'dart:io';

import 'Hero.dart';
import 'Object.dart';

class Dragon implements Hero, Object {
  int dmg = 35;
  int health = 120;
  String? name;
  int damageUltimate = 150;
  String nameUltimate = "Firefly";

  Dragon(String name) {
    this.name = name;
    this.health = health;
    this.dmg = dmg;
    this.damageUltimate;
  }

  @override
  attacked(int damaged) {
    health = health - damaged;
    print("Hero was attacked by monster " + damaged.toString() + " damage");
    return health;
  }

  @override
  die() {
    if (health <= 0) {
      return true;
    } else {
      return false;
    }
  }

  @override
  gotUltimate(int damageUltimate) {
    health = health - damageUltimate;
    print("Hero was attacked by ultimate from monster " +
        damageUltimate.toString() +
        " damage");
    return health;
  }

  @override
  fly() {
    print("You can't hide from me because I can fly to kill you");
  }
}
